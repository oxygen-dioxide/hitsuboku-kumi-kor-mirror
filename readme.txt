�M�n�N�~ ~ Korean

Thank you for downloading!

https://cubialpha.wixsite.com/koomstar
____________________________

Notes
> Possible mispronunciations.
> Non-standard reclist.

Optimal Settings
> Range: C3-C6
> Flags: None
> Resampler: tn_fnds, TIPS, moresampler*(with moresampler as wavtool)

Recordings
> 2-Syllable CVVC
> V-V Transitions
> V+Sustained Consonant Transitions
> Additional English Recordings

Pitches
> 4 (A3, D4, A4, D5)

Features & Usage
> "l", "m", "n", "ng" + Vowel transitions (ng a, n l, etc).
> Various English CV,VC,VC- add-ons (f, v, z, th, sh).
> Vowel & l,m,n,ng ending aliases (a!, a-, aR, ng-, etc).
> CV aliases (-ba, ba, etc).
> VC aliases (ab, etc)
> Extra English VC- (af-, av-, az-, ath-, ash-, etc)

Details
> Recording Date: 7/8/2017
> Overall Quality: Good
> Download Size: 267 MB

Credits
> Created by Cubialpha (@_cubialpha)
> Special Thanks to @opcbyul, WintermintP